# thesis_template

Yet another LaTeX thesis template, based on [memoir](https://ctan.org/pkg/memoir?lang=en). I may (probably not) add to this over time.

Most of the formatting has been inherited from other places. Where relevant, references to the original author have been included in the file(s)

## Usage
Call `make` to call with draft mode enabled by default. This prints the pdf document with line numbers
Call `make nodraft` to call without line numbers printed
`make check` and `make check_references` will run the spell check script `checkwriting`

## CI
I also included a basic CI which will check the LaTeX compiles

## Examples
Some examples of the output can be found below. A full pdf output can be downloaded [here](thesis.pdf)

Title page:
![Title page](examples/title_page.png)

Table of contents:
![Table of contents](examples/TOC.png)

Beginning of chapter:
![Chapter beginning](examples/test_chapter.png)

Example subsection:
![Subsection](examples/test_subsection.png)

Example of maths:
![Maths](examples/test_maths.png)
